<?php

namespace Project\Search;

use CIBlockElement,
    CIBlockSection;

class Section {

    static public function getSubSection($ID) {
        static $sections = array();
        if (empty($sections[$ID])) {
            $sections[$ID] = array(
                $ID => $ID
            );
            $rsParentSection = CIBlockSection::GetByID($ID);
            if ($arParentSection = $rsParentSection->GetNext()) {
                $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
                $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array('ID'));
                while ($arSect = $rsSect->GetNext()) {
                    $sections[$ID][$arSect['ID']] = $arSect['ID'];
                }
            }
        }
        return $sections[$ID];
    }

    static public function isSection($ID, $noSection) {
        $sections = self::getSubSection($noSection);
        $res = CIBlockElement::GetElementGroups($ID, true, array('ID'));
        while ($arGroup = $res->Fetch()) {
            if (in_array($arGroup['ID'], $noSection)) {
                return true;
            }
        }
        return false;
    }

}
