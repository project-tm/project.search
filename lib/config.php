<?php

namespace Project\Search;

class Config {

    const BRAND = [];
	const NO_SECTION = [5];
    const CATALOG_ID = \Tm\Opticsite\Iblock\Config::CATALOG_ID;
    const OFFERS_ID = \Tm\Opticsite\Iblock\Config::OFFERS_ID;

}
