<?

namespace Project\Search;

use CIBlockProperty,
    Project\Core\Utility;

class Props {

    public static function getList($iblock) {
        static $cache = array();
        if (!isset($cache[$Iblock])) {
            $cache[$Iblock] = Utility::useCache(array(__CLASS__, __FUNCTION__, $iblock), function() use($iblock) {
                        $arResult = array();
                        $res = CIBlockProperty::GetList(Array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock, 'SEARCHABLE' => 'Y'));
                        while ($arItem = $res->fetch()) {
                            $arResult[] = 'PROPERTY_'. strtoupper($arItem['CODE']);
                        }
                        return $arResult;
                    });
        }
        return $cache[$Iblock];
    }

}
