<?

namespace Project\Search;

use CIBlockElement;

class Event {

    public static function BeforeIndex($arFields) {
        if ($arFields['MODULE_ID'] === 'iblock' and $arFields['ITEM_ID'] and $arFields['PARAM2'] == Config::CATALOG_ID) {

            /* артикул */
            $arSearch = Props::getList($arFields['PARAM2']);
            $arSelect = array_merge($arSearch, Array(
                'NAME',
                'PROPERTY_ARTNUMBER',
            ));
            $arFilter = Array("IBLOCK_ID" => $arFields['PARAM2'], "ID" => $arFields['ITEM_ID']);
            $atItem = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect)->Fetch();
            $search = $atItem['NAME'];
            if ($atItem) {
                foreach ($arSearch as $value) {
                    if (!empty($atItem[$value . '_VALUE'])) {
                        $search .= ' ' . $atItem[$value . '_VALUE'];
                    }
                }
                $search .= ' ' . preg_replace('~([^0-9]+)~', '', $atItem['PROPERTY_ARTNUMBER_VALUE']);
            }
            $arFields['TITLE'] = $arFields['BODY'] = $search;

            /* цена */
            $arSelect = Array(
                'ACTIVE',
                'IBLOCK_SECTION_ID',
                'PREVIEW_PICTURE',
                'DETAIL_PICTURE',
                'DETAIL_TEXT',
                'PREVIEW_TEXT',
                'PROPERTY_MORE_PHOTO',
                'PROPERTY_ARTNUMBER',
                'CATALOG_GROUP_1',
            );
            $arFilter = Array("IBLOCK_ID" => $arFields['PARAM2'], "ID" => $arFields['ITEM_ID']);
            $atItem = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect)->Fetch();
            $arFields['BODY'] .= ' ' . $atItem['TITLE'];
            $arFields['TITLE'] .= ' ' . $atItem['PREVIEW_TEXT'] . ' ' . $atItem['PROPERTY_ARTNUMBER_VALUE'];
            if ((empty($atItem['DETAIL_PICTURE']) and empty($atItem['PREVIEW_PICTURE']) and empty($atItem['PROPERTY_MORE_PHOTO_VALUE'])) or $atItem['ACTIVE'] == 'N' or $atItem['CATALOG_PRICE_1'] < 1) {
                $arFields['TITLE'] = $arFields['BODY'] = '';
            }

            /* разделы */
            if (Section::isSection($arFields['ITEM_ID'], Config::NO_SECTION)) {
                $arFields['TITLE'] = $arFields['BODY'] = '';
            }
        }
//        preExit($arFields);
        return $arFields;
    }

}
